"""
This module contains Argot extension to the Language Server protocol

https://grammatech.gitlab.io/Mnemosyne/docs/architecture/
"""

import enum
from typing import Any, Dict, List, Optional, Union

from pygls.lsp.types.basic_structures import Model, Range, TextDocumentIdentifier


class ArgotKind(enum.Enum):
    Code = "code"
    Types = "types"
    Test = "tests"
    Prose = "prose"


class Contribution(Model):
    source: str
    key: Union[int, str]
    kind: ArgotKind
    data: Dict[str, Any]


class Annotation(Model):
    text_document: TextDocumentIdentifier
    range: Optional[Range]
    contributions: List[Contribution]


class GetAnnotationsParams(Model):
    text_document: TextDocumentIdentifier
    range: Optional[Range]


class DidAnnotateParams(Model):
    annotations: List[Annotation]


class ArgotFilesParams(Model):
    base: Optional[str]


class ArgotContentParams(Model):
    text_document: TextDocumentIdentifier
